using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugApplication
{

    public partial class Tester : Form
    {
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=E:\TBC\Bug tracking\BugApplication\BugApplication\BugDatabase.sdf");
        string pid;
        string userid;

        public Tester(string getId)
        {
            InitializeComponent();
            userid = getId;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();

            string productname = pname.Text;
            string prodDesc = pDesc.Text;
            string uid = userid;
            


            string sqlquery = "Insert into Product_Table(Product_Name,Description,User_ID)" + "Values(@productname, @prodDesc,@userid)";
            SqlCeCommand cmd = new SqlCeCommand(sqlquery, conn);
            cmd.Parameters.AddWithValue("@productname", productname);
            cmd.Parameters.AddWithValue("@prodDesc", prodDesc);
            cmd.Parameters.AddWithValue("@userid", uid);
            try
            {
                int affectedRows = cmd.ExecuteNonQuery();
                conn.Close();
                if (affectedRows > 0)
                {
                    MessageBox.Show("Insert Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

            string bugTitle = bugtitle.Text;
            string bugDesc = bugdesc.Text;
            string proid = productid.Text;

            string sqlquery = "Insert into Bug_Table(Bug_Title,Bug_Description,Product_ID)" + "Values(@bugtitle, @bugDesc,@prodid)";
            SqlCeCommand cmd = new SqlCeCommand(sqlquery, conn);
            cmd.Parameters.AddWithValue("@bugtitle", bugTitle);
            cmd.Parameters.AddWithValue("@bugDesc", bugDesc);
            cmd.Parameters.AddWithValue("@prodid", proid);
            try
            {
                conn.Open();
                int affected = cmd.ExecuteNonQuery();
                conn.Close();

                if (affected > 0)
                {
                    MessageBox.Show("Bug Insert Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Tester_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login file = new Login();
            file.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            string Sql = "select * from Product_Table where Product_Name= '"+comboBox1.Text+"' ";

            SqlCeCommand cmd2 = new SqlCeCommand(Sql, conn);
            SqlCeDataReader DR = cmd2.ExecuteReader();

            while (DR.Read())
            {
                pid = DR.GetInt32(0).ToString();
                productid.Text = pid;
            }
            conn.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            conn.Open();
            groupBox2.Visible = true;
            groupBox1.Enabled = false;
            string Sql = "select Product_Name from Product_Table";

            SqlCeCommand cmd1 = new SqlCeCommand(Sql, conn);
            SqlCeDataReader DR = cmd1.ExecuteReader();

            while (DR.Read())
            {
                comboBox1.Items.Add(DR["Product_Name"].ToString());

            }
            conn.Close();
        }
    }
}

