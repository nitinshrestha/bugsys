using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugApplication
{
    public partial class Tester : Form
    {
        public SqlCeConnection conn = new SqlCeConnection(@"Data Source=E:\TBC\Bug tracking\BugApplication\BugApplication\BugDatabase.sdf");
        public Tester()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {

            conn.Open();
            string bugID = bug.Text;
            string productID = product.Text;
            string bugDesc = desc.Text;
            

            string sqlquery = "Insert into Bug_Table(Bug_ID,Product_ID,Bug_Description)" + "Values(@bugID, @productID, @bugDesc)";
            SqlCeCommand cmd = new SqlCeCommand(sqlquery, conn);
            cmd.Parameters.AddWithValue("@bugID", bugID);
            cmd.Parameters.AddWithValue("@productID", productID);
            cmd.Parameters.AddWithValue("@bugDesc", bugDesc);
           
            try
            {
                int affectedRows = cmd.ExecuteNonQuery();
                if (affectedRows > 0)
                {
                    MessageBox.Show("Insert Success", Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Information);
                }

            }
            catch (Exception){
                MessageBox.Show("Insert Failed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                conn.Close();
            }
        }
        

        private void Tester_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        }
    }
