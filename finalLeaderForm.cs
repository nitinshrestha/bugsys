using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugApplication
{
    public partial class Leader : Form
    {
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=E:\TBC\Bug tracking\BugApplication\BugApplication\BugDatabase.sdf");
        SqlCeDataAdapter adapter;
        string BugId;
        public Leader()
        {
            InitializeComponent();
            bug();
        }
        DataTable dt = new DataTable();

        public void bug()
        {

            conn.Open();
            string query = "Select Username from Users where role='Developer'";
            SqlCeCommand cmd = new SqlCeCommand(query, conn);
            SqlCeDataReader da = cmd.ExecuteReader();

            while (da.Read())
            {
                string assign = da["Username"].ToString();
                comboBox1.Items.Add(assign);
            }
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fillDataGrid();
        }
        private void fillDataGrid()
        {
        
            try
            {
                conn.Open();
                SqlCeCommand cmd = new SqlCeCommand("Select Users.Username,Product_Table.Product_ID,Product_Table.Product_Name,Bug_Table.Bug_Title,Bug_Table.Bug_Description FROM Product_Table inner join Users on Users.User_ID= Product_Table.User_ID inner join Bug_Table on Product_Table.Product_ID=Product_Table.Product_ID", conn);
                SqlCeDataReader reader = cmd.ExecuteReader();
               
                adapter = new SqlCeDataAdapter(cmd);
                adapter.Fill(dt);
                gridview.DataSource = dt;
                gridview.DataMember = dt.TableName;
                
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message);
            }
            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();
            string assign = comboBox1.Text;
            SqlCeCommand cmd = new SqlCeCommand("INSERT INTO Bug_Assign (Assignto,Bug_ID) VALUES (@assign, @bugid)", conn);

            cmd.Parameters.AddWithValue("@assign", assign);
            cmd.Parameters.AddWithValue("@bugid", BugId);

            int affectedRows = cmd.ExecuteNonQuery();

            if (affectedRows > 0)
            {
                MessageBox.Show("Assign Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Assign  Failed", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            conn.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            conn.Open();
            SqlCeCommand cmd = new SqlCeCommand("Select * FROM Bug_Table", conn);
            SqlCeDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                BugId = reader["Bug_ID"].ToString();

                if (e.RowIndex >= 0)
                {
                    DataGridViewRow row = this.gridview.Rows[e.RowIndex];

                    bugtitle.Text = row.Cells["Bug_Title"].Value.ToString();
                    bugDesc.Text = row.Cells["Bug_Description"].Value.ToString();
                    proid.Text = row.Cells["Product_ID"].Value.ToString();
                }
            }
            conn.Close();
        }

        private void productid_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login file = new Login();
            file.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }
}


