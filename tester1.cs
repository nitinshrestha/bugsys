using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugApplication
{

    public partial class Tester : Form {
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=E:\TBC\Bug tracking\BugApplication\BugApplication\BugDatabase.sdf");

        public Tester()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            conn.Open();
            string productname = pname.Text;
            string prodDesc = pDesc.Text;


            string sqlquery = "Insert into Product_Table(Product_Name,Description)" + "Values(@productname, @prodDesc)";
            SqlCeCommand cmd = new SqlCeCommand(sqlquery, conn);
            cmd.Parameters.AddWithValue("@productname", productname);
            cmd.Parameters.AddWithValue("@prodDesc", prodDesc);
            try
            {
                int affectedRows = cmd.ExecuteNonQuery();
                conn.Close();
                if (affectedRows > 0)
                {
                    MessageBox.Show("Insert Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    groupBox2.Visible = true;
                    groupBox1.Enabled = false;
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            string bugID = bugid.Text;
            string bugDesc = bugdesc.Text;


            string sqlquery = "Insert into Bug_Table(Bug_ID,Bug_Description)" + "Values(@bugID,@bugDesc)";
            SqlCeCommand cmd = new SqlCeCommand(sqlquery, conn);
            cmd.Parameters.AddWithValue("@bugID", bugID);
            cmd.Parameters.AddWithValue("@bugDesc", bugDesc);
            try
            {
                conn.Open();
                int affected = cmd.ExecuteNonQuery();
                conn.Close();
    
                if (affected > 0)
                {
                    MessageBox.Show("Bug Insert Success", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Tester_Load(object sender, EventArgs e)
        {

        }
    }
}
