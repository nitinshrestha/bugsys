using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BugApplication
{
    public partial class Developer : Form
    {
        SqlCeConnection conn = new SqlCeConnection(@"Data Source=E:\TBC\Bug tracking\BugApplication\BugApplication\BugDatabase.sdf");
        string ID;
        public Developer(string userID)
        {
            InitializeComponent();

            ID = userID;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login file = new Login();
            file.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string m = "SELECT * FROM Bug_Table";
            SqlCeCommand m1 = new SqlCeCommand(m, conn);
            conn.Open();
            SqlCeDataReader dr = m1.ExecuteReader();

            while (dr.Read())
            {
                int i = e.RowIndex;
                DataGridViewRow row = dataGridView1.Rows[i];

                textBox2.Text = row.Cells["Bug_Title"].Value.ToString();
                textBox3.Text = row.Cells["Bug_Description"].Value.ToString();


            }
            conn.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conn.Open();
            SqlCeCommand cmd= new SqlCeCommand("Select * FROM Bug_Table",conn);
            
            SqlCeDataAdapter data = new SqlCeDataAdapter(cmd);
            DataTable dt = new DataTable();
            data.Fill(dt);
            BindingSource var = new BindingSource();
            var.DataSource = dt;
            dataGridView1.DataSource = var;
            data.Update(dt);
            conn.Close();
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string comment = textBox1.Text;
            conn.Open();

                string m = "INSERT INTO Bug_Solution ([Comment]) VALUES (@comment)";
                SqlCeCommand c = new SqlCeCommand(m, conn);
                c.Parameters.AddWithValue("@comment", comment);
               ;
                int row = c.ExecuteNonQuery();
                if (row > 0)
                {
                    MessageBox.Show("Solution Added");
                   


                }
                else
                {
                    MessageBox.Show("ERROR");
                }
                conn.Close();
            }
  
    }
}

